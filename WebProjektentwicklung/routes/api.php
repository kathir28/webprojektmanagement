<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['api', 'cors']], function() {
    Route::post('auth/login', 'Auth\ApiAuthController@login');

    Route::get('books', 'BookController@index');
    Route::get('book/{isbn}', 'BookController@findByISBN');
    Route::get('book/checkisbn/{isbn}', 'BookController@checkISBN');
    Route::get('books/search/{searchTerm}', 'BookController@findBySearchTerm');
    Route::get('cart', 'BookController@index');


});


// methods with auth
Route::group(['middleware' => ['api', 'cors', 'jwt-auth']], function() {


// update book
    Route::put('book/{isbn}', 'BookController@update');
// delete book
    Route::delete('book/{isbn}', 'BookController@delete');


// insert book
    Route::post('book', 'BookController@save');

    Route::post('auth/logout', 'Auth\ApiAuthController@logout');
    Route::post('auth/user', 'Auth\ApiAuthController@getCurrentAuthenticationUser');
    Route::get('auth/user/{id}', 'Auth\ApiAuthController@getRights');
    Route::get('auth/admin/{id}', 'Auth\ApiAuthController@getRights');

    // update order status
    // Route::put('order/{id}', 'OrderController@changeStatus');
    //get taxes
    Route::get('orders/all', 'OrderController@index');
    Route::get('orders/all/{id}', 'OrderController@getOrders');
    Route::get('orders/all/{type}', 'OrderController@sortOrders');
    Route::get('orders/{id}', 'OrderController@getCartItems');
    Route::get('order/{id}', 'OrderController@getCartItems');
    Route::put('order/{id}', 'OrderController@addTax');
    Route::post('orders', 'OrderController@changeStatus');
    Route::post('order/create', 'OrderController@newOrder');
    Route::get('status/{id}', 'OrderController@getStatusHistory');
    Route::get('cart-items/{id}', 'OrderController@getCartItemBooks');
});

