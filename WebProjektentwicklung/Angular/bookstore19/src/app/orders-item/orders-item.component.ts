import {Component, Input, OnInit} from '@angular/core';
import {Order} from "../shared/order";
import {BookStoreService} from "../shared/book-store.service";
import {Router} from "@angular/router";

@Component({
  selector: 'p.bs-orders-item',
  templateUrl: './orders-item.component.html',
  styles: []
})
export class OrdersItemComponent implements OnInit {
  @Input() order: Order;
  statuses = [
      "offen",
      "bezahlt",
      "versendet",
      "storniert"
  ];

  constructor(
      private bs: BookStoreService,
      private router: Router,
  ) {

  }

  ngOnInit() {
  }

  changeStatus() {
    let data = {
      id: this.order.id,
      status: this.order.status,
      comment: this.order.comment
    };
    this.bs.changeStatus(data).subscribe((res) => {
              this.router.navigateByUrl("/orders");
              });
    console.log('Hello');
  }

}
