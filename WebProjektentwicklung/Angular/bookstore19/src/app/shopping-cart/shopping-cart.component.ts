import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../shared/book';
import {ActivatedRoute, Router} from "@angular/router";
import { BookStoreService } from "../shared/book-store.service";
import {AuthService} from "../shared/authentication-service";
import {CartItem} from "../shared/cart-item";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'bs-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styles: []
})
export class ShoppingCartComponent implements OnInit {
    @Input() cartItems : CartItem[] = [];
    @Input() books : Book[] = [];

  constructor(
      private bs: BookStoreService,
      private router: Router,
      private route : ActivatedRoute,
      private authService : AuthService
  ) { }

  ngOnInit() {
    // var l = localStorage.length;
      var counter = JSON.parse(localStorage.getItem('counter'));

    var retrievedObject = localStorage.getItem('books');
    if(retrievedObject != null || retrievedObject != undefined) {
        var items = JSON.parse(retrievedObject);
        var l = Object.keys(items).length;
        for (var i = 1; i < l+1; i++) {
            const it = items[i];

            this.bs.getSingle(it).subscribe(
                res => this.cartItems.push(new CartItem(res, 1)));
            console.log(this.cartItems);
            /* this.bs.getSingle(it).subscribe(
                res => this.books.push(res)
            );*/
        };
        this.getSum();
    }

  }

  getSum(){
      var sum = 0;
      this.cartItems.forEach(function (item) {
          console.log('Hello');
          // sum = sum + item.book.price;
          console.log(item);
      });
      // console.log(sum);
      return sum;
  }

  gatItem($items, $id){
      var book = CartItem;
      $items.forEach(function (item) {
          if(item.book.id == $id){
              console.log(item['book']);
              book = item;
          }
      });
      return book;
  }

  removeBook($id, $isbn){
      var items = this.cartItems;
      var book =  this.gatItem(items, $id);
      this.cartItems.splice( this.cartItems.indexOf(book['book']), 1 );
      console.log(this.cartItems);


      var data = {};
      var retrievedObject = localStorage.getItem('books');
      /*if(retrievedObject != null || retrievedObject != undefined) {
          var books = JSON.parse(retrievedObject);
          var l = Object.keys(books).length;
          var counter = 1;
          for (var i = 1; i < l + 1; i++) {
              console.log(books);
              //localStorage.removeItem('books')
              if(books[i] !== $isbn){
                  data[counter] = books[i];
                  counter = counter +1;
                  localStorage.setItem('books', JSON.stringify(data));
              }
          }
          console.log(localStorage)
      }*/
  }


  buyItems(){
        let data = {};
      var user_id = JSON.parse(localStorage.getItem('userId'));
        this.cartItems.forEach(function (item) {
            data[item.book.id] = {'quantity' : item.quantity, 'userId' : user_id};
        });
        
      this.bs.createOrder(data).subscribe((res) => {
          // todo use router to go to orders page
          this.router.navigateByUrl("/order");
      });

      //für jedes CartItem Menge berechnen und dann neue CartItems anlegen im Backend und Order anlegen
  }

    getRating (num: number) {
        return new Array(num);
    }


}
