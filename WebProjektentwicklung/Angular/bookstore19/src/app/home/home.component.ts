import {Component, Input, OnInit, Output} from '@angular/core';
import {BookStoreService} from "../shared/book-store.service";
import {Book} from "../shared/book";

@Component({
  selector: 'bs-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  @Input() books : Book[] = [];
  constructor(
      private bs : BookStoreService,
  ) { }

  ngOnInit() {
    this.bs.searchForBooks("Herr").subscribe(res => this.books = res);
  }

}
