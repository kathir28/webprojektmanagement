import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersDetailDetailsComponent } from './orders-detail-details.component';

describe('OrdersDetailDetailsComponent', () => {
  let component: OrdersDetailDetailsComponent;
  let fixture: ComponentFixture<OrdersDetailDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersDetailDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersDetailDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
