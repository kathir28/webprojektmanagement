import {Component, Input, OnInit} from '@angular/core';
import {Book} from "../shared/book";
import {CartItem} from "../shared/cart-item";

@Component({
  selector: 'p.bs-orders-detail-details',
  templateUrl: './orders-detail-details.component.html',
  styles: []
})
export class OrdersDetailDetailsComponent implements OnInit {

  @Input() item: CartItem;
  constructor() { }

  ngOnInit() {
  }

}
