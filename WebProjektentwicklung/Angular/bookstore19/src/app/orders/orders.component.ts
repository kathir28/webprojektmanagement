import { Component, OnInit } from '@angular/core';
import {BookStoreService} from "../shared/book-store.service";
import {Order} from "../shared/order";
import {CartItem} from "../shared/cart-item";

@Component({
  selector: 'bs-orders',
  templateUrl: './orders.component.html',
  styles: []
})
export class OrdersComponent implements OnInit {

  orders : Order[];
  cartItems : CartItem[];

  constructor(private bs : BookStoreService) { }

  ngOnInit() {
    this.bs.getAllOrders().subscribe(res => this.orders = res);
    //this.bs.getCartItems($id).subscribe(res => this.cartItems = res);
  }



}