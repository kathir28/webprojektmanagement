import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../shared/book';
import {ActivatedRoute, Router} from "@angular/router";
import { BookStoreService } from "../shared/book-store.service";
import {AuthService} from "../shared/authentication-service";
import NumberFormat = Intl.NumberFormat; [NumberFormat];

@Component({
    selector: 'bs-book-details',
    templateUrl: './book-details.component.html',
    styles: []
})
export class BookDetailsComponent implements OnInit {

    @Input() book : Book;

    constructor(
        private bs: BookStoreService,
        private router: Router,
        private route : ActivatedRoute,
        private authService : AuthService,

    ) { }

    ngOnInit() {
        const params = this.route.snapshot.params;
        this.bs.getSingle(params['isbn']).subscribe(
            datafromObservable => this.book = datafromObservable
        );
        console.log(params['isbn']);

    }

    removeBook () {
        if (confirm("Buch wirklich löschen?")) {
            this.bs.remove(this.book.isbn)
                .subscribe(res => this.router.navigate(['../'],
                    { relativeTo: this.route }
                ));
        }
    }

   addtoCart () {
        const params = this.route.snapshot.params;
        var item = JSON.parse(localStorage.getItem('counter'));
        console.log(item);
       var books = JSON.parse(localStorage.getItem('books'));
       console.log(books);
       for (var i = 0; i < item; i++) {
           const it = books[i];
           console.log(it);
           if(params['isbn'] === it) {
               alert('Book already exits in your cart! Please change the quantity in your cart!');
               return;
           }
           if(params['isbn'] !== it) {
               books[item] = params['isbn'];
               console.log('added!');
           }
       };

       localStorage.setItem('books', JSON.stringify(books));
        // counter
        var newItem = item +1;
       localStorage.setItem('counter', JSON.stringify(newItem));
        //console.log(localStorage);
    }



    getRating (num: number) {
        return new Array(num);
    }


}