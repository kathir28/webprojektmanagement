import {Component, Input, OnInit} from '@angular/core';
import {Order} from "../shared/order";

@Component({
  selector: 'p.bs-order-item',
  templateUrl: './order-item.component.html',
  styles: []
})
export class OrderItemComponent implements OnInit {

  @Input() order: Order;
  constructor() { }

  ngOnInit() {
  }

}
