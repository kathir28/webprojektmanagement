import {Component, Input, OnInit} from '@angular/core';
import {Order} from "../shared/order";
import {BookStoreService} from "../shared/book-store.service";

@Component({
  selector: 'bs-order',
  templateUrl: './order.component.html',
  styles: []
})
export class OrderComponent implements OnInit {
  @Input() orders: Order[] = [];
  constructor(
      private bs : BookStoreService,
  ) { }

  ngOnInit() {
    var userId = JSON.parse(localStorage.getItem('userId'));
    this.bs.getOrders(userId).subscribe(res => this.orders = res);
  }

}
