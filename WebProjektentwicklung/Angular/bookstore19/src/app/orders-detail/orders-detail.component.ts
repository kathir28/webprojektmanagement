import {Component, Input, OnInit} from '@angular/core';
import {CartItem} from "../shared/cart-item";
import {BookStoreService} from "../shared/book-store.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../shared/authentication-service";
import {Status} from "../shared/status";
import {Book} from "../shared/book";

@Component({
  selector: 'bs-orders-detail',
  templateUrl: './orders-detail.component.html',
  styles: []
})
export class OrdersDetailComponent implements OnInit {

  @Input() cartItems : CartItem[] = [];
  @Input() books : Book[] = [];
  @Input() statuses : Status[] = [];

  constructor(
      private bs: BookStoreService,
      private router: Router,
      private route : ActivatedRoute,
      private authService : AuthService,

  ) { }

  ngOnInit() {
    const params = this.route.snapshot.params;
    //get an Array of Books from the Order
    this.bs.getCartItemBooks(params['id']).subscribe(res => this.books = res);

    this.bs.getCartItems(params['id']).subscribe(res => this.cartItems = res);

    this.bs.getStatusHistory(params['id']).subscribe(res => this.statuses = res);

  }

}
