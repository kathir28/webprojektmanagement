import { Component, OnInit, Input } from '@angular/core';
import {Book} from '../shared/book';
@Component({
  selector: 'div.bs-home-item',
  templateUrl: './home-item.component.html',
  styles: []
})
export class HomeItemComponent implements OnInit {
  @Input() book : Book;
  constructor() { }

  ngOnInit() {
  }

}
