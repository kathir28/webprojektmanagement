
export class Order {
    constructor (
        public id : number,
        public date : Date,
        public total : number,
        public status : string,
        public user_id : number,
        public comment : string
    ) {}
}
