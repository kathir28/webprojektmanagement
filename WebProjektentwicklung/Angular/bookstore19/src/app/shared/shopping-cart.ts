import {Injectable} from '@angular/core';
import {isNullOrUndefined} from "util";
import {HttpClient} from "@angular/common/http";
import {retry} from 'rxjs/operators';
import {Author} from "./author";
import {Image} from "./image";
import {Book} from "./book";


/* interface Book {
    result: {
        created_at: Date,
       id: number,
        isbn: string,
        title: string,
        authors : Author[],
        published: Date,
        price: number,
        user_id : number,
        subtitle? : string,
        rating? : number,
        images? : Image[],
        description?: string,
        updated_at: Date
    }
}*/


@Injectable()
export class ShoppingCart {
    constructor (
        public id: number,
        public date: Date,
        public total: number,
        public user_id: number

    ) { }

    private api:string = '\'http://bookstore19.s1610456028.student.kwmhgb.at/api/cart';//'http://localhost:8080/api/cart';


    public setCurrentTotal(){
        localStorage.setItem('total', this.total.toString());

    }

    public getCurrentTotal(){
        return Number.parseInt(localStorage.getItem('total'));
    }

    /* public setLocalStorage(token: string) {
        console.log("Storing token");
        console.log(token);
        const decodedToken = decode(token);
        console.log(decodedToken);
        console.log(decodedToken.user.id);
        localStorage.setItem('token', token);
        localStorage.setItem('userId', decodedToken.user.id);
    }

    public logout() {
        console.log('logout');
        this.http.post(`${this.api}/logout`, {});
        localStorage.removeItem("token");
        localStorage.removeItem("userId");
        console.log("logged out");
    }*/


}