import {Injectable} from '@angular/core';
import {isNullOrUndefined} from "util";
import {HttpClient} from "@angular/common/http";
import * as decode from 'jwt-decode';
import {catchError, retry} from 'rxjs/operators';

//npm install --save-dev jwt-decode

interface User {
    result: {
        created_at: Date,
        email: string,
        id: number,
        name: string,
        admin: boolean,
        updated_at: Date
    }
}

@Injectable()
export class AuthService {

    private api:string = 'http://bookstore19.s1610456028.student.kwmhgb.at/api/auth';//'http://localhost:8080/api/auth';

    constructor(private http: HttpClient) {
    }

    login(email: string, password: string ) {
        return this.http.post(`${this.api}/login`, {'email': email, 'password': password});
    }

    public setCurrentUserId(){
        this.http.get<User>(`${this.api}/user`).pipe(retry(3)).subscribe(res =>{
                localStorage.setItem('userId', res.result.id.toString());
            }
        );
    }

    public setCurrentUserRights(){
        return this.http.get<User>(`${this.api}/user`).pipe(retry(3)).subscribe(res =>{
                localStorage.setItem('admin', res.result.admin.toString());
            }
        );
    }

    public getCurrentUserId(){
        return Number.parseInt(localStorage.getItem('userId'));
    }

    public setLocalStorage(token: string) {
        console.log("Storing token");
        console.log(token);
        const decodedToken = decode(token);
        console.log(decodedToken);
        console.log(decodedToken.user.id);
        localStorage.setItem('token', token);
        localStorage.setItem('userId', decodedToken.user.id);
        if(decodedToken.user.id == 1){
            localStorage.setItem('admin', '1');
        }
        localStorage.setItem('counter', "1");
        var books = { };
        localStorage.setItem('books', JSON.stringify(books));
        var cartItems = { };
        localStorage.setItem('cartItems', JSON.stringify(cartItems));
    }

    public logout() {
        console.log('logout');
        this.http.post(`${this.api}/logout`, {});
        localStorage.removeItem("token");
        localStorage.removeItem("userId");
        localStorage.removeItem('admin');
        console.log("logged out");
    }

    public isLoggedIn() {
        console.log(localStorage);
        return !isNullOrUndefined(localStorage.getItem("token"));
    }

    public isAdmin() {
        return localStorage.getItem("admin") == '1';
    }


    isLoggedOut() {
        return !this.isLoggedIn();
    }

}