
import {Book} from "./book";

export class CartItem {
    constructor (
        public book : Book,
        public quantity : number

    ) {}

    getId() {
        return this.book.getId();
    }

    getBookTitle(){
        return this.book.title;
    }

}

