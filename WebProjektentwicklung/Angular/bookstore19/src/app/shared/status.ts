
export class Status {
    constructor (
        public id : number,
        public order_id : number,
        public status: string,
        public comment : string,
        public created_at : Date,
    ) {}
}
