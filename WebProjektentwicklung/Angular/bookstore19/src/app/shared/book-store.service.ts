import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import {Author, Book, Image} from "./book";
import {Order} from "./order";
import {CartItem} from "./cart-item";

@Injectable()
export class BookStoreService {

    private api = 'http://bookstore19.s1610456028.student.kwmhgb.at/api';
    books : Book[];
    orders : Order[];
    constructor(private http: HttpClient) {
        // localStorage.setItem('counter', "1");
        // var books = { };
        // localStorage.setItem('books', JSON.stringify(books));
    }

    getAll() : Observable<Array<Book>> {
        return this.http.get(`${this.api}/books`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));

    }

    getSingle(isbn : string) : Observable<Book> {
        return this.http.get(`${this.api}/book/${isbn}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    searchForBooks(searchTerm : string){
        return this.http.get(`${this.api}/books/search/${searchTerm}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    create( book: Book) : Observable<any> {
        return this.http.post(`${this.api}/book`, book)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    update( book: Book) : Observable<any> {
        return this.http.put(`${this.api}/book/${book.isbn}`, book)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    remove(isbn: string) : Observable<any> {
        return this.http.delete(`${this.api}/book/${isbn}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    createOrder(data: object) {
        return this.http.post(`${this.api}/order/create`, {items: data})
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    changeStatus(data: object){
        return this.http.post(`${this.api}/orders`, data);
    }

    getStatusHistory(id : number){
        return this.http.get(`${this.api}/status/${id}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    //die getCartItemBooks liefert zu jeder Bestellposition das Buch und dessen Attribute.
    getCartItemBooks(id : number){
        return this.http.get(`${this.api}/cart-items/${id}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    // die getOrders Funktion gibt die Bestellungen eines Benutzers aus.
    getOrders(id : number) : Observable<Array<Order>>{
        return this.http.get(`${this.api}/orders/all/${id}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }


    getAllOrders() : Observable<Array<Order>>{
        return this.http.get(`${this.api}/orders/all`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    sortOrders(type : string) : Observable<Array<Order>>{
    return this.http.get(`${this.api}/orders/all/${type}`)
    .pipe(retry(3))
    .pipe(catchError(this.errorHandler));
    }

    //die get CartItems liefert für jede Bestellung die dazugehörigen Bestellpositionen.
    getCartItems(id : number) : Observable<Array<CartItem>>{
        return this.http.get(`${this.api}/orders/${id}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    private errorHandler ( error:Error | any) : Observable<any> {
        return throwError(error);
    }
}

