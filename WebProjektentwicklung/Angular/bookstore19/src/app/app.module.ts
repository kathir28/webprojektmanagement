import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookListItemComponent } from './book-list-item/book-list-item.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookStoreService} from "./shared/book-store.service";
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from "./app-routing.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import { BookFormComponent } from './book-form/book-form.component';
import { LoginComponent } from './admin/login/login.component';
import {AuthService} from "./shared/authentication-service";
import {TokenInterceptorService} from "./shared/token-interceptor.service";
import {JwtInterceptorService} from "./shared/jwt-interceptor.service";
import { ProfileComponent } from './profile/profile.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { OrderComponent } from './order/order.component';
import { OrdersComponent } from './orders/orders.component';
import { OrdersItemComponent } from './orders-item/orders-item.component';
import { OrdersDetailComponent } from './orders-detail/orders-detail.component';
import { OrdersDetailDetailsComponent } from './orders-detail-details/orders-detail-details.component';
import { HomeItemComponent } from './home-item/home-item.component';
import { OrderItemComponent } from './order-item/order-item.component';

@NgModule({
    declarations: [
        AppComponent,
        BookListComponent,
        BookListItemComponent,
        BookDetailsComponent,
        HomeComponent,
        BookFormComponent,
        LoginComponent,
        ProfileComponent,
        ShoppingCartComponent,
        OrderComponent,
        OrdersComponent,
        OrdersItemComponent,
        OrdersDetailComponent,
        OrdersDetailDetailsComponent,
        HomeItemComponent,
        OrderItemComponent,
    ],
    imports: [
        BrowserModule, FormsModule, AppRoutingModule, HttpClientModule, CommonModule,
        ReactiveFormsModule
    ],
    providers: [
        BookStoreService, AuthService, {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptorService,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptorService,
            multi: true
        }

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
