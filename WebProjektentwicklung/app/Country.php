<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Country extends Model
{
    protected $fillable = [
        'id', 'name', 'tax'
    ];

    public function adress(): HasMany {
        return $this->hasMany(Address::class);
    }
}