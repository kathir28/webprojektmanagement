<?php

namespace App;

use App\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class OrderStatus extends Model
{
    protected $fillable = [
        'id', 'order_id', 'status', 'comment'
    ];

    public function order() : BelongsTo  {
        return $this->belongsTo(Order::class);
    }

}