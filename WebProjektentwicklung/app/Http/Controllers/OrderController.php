<?php
namespace App\Http\Controllers;

use App\Author;
use App\Order;
use App\OrderStatus;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

use App\Book;
use App\User;
use App\CartItem;
use Mockery\Exception;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{

    public function index() {
        // todo nach datum und user sortiert
        $orders = Order::with(['user'])->orderBy('date', 'desc')->get();
        return $orders;
    }

    public function sortOrders(string $type) {
        if($type == 'benutzer'){
            $orders = Order::with(['user'])->orderBy('user_id', 'asc')->get();
            return $orders;
        }
        if($type == 'status'){

        }
    }

    public function getOrders(string $userId){
        $order = Order::query()->where('user_id', "=", $userId)->get();
        return $order;
    }

    public function getCartItems(string $id) {
        $cartItems = CartItem::query()->where('order_id', "=", $id)->getModels();

        foreach($cartItems as $cartItem) {
            $book = Book::query()->find($cartItem->book_id);;

            $book->authors = Author::query()->join("author_book", "author_book.author_id", "=", "authors.id")->where("author_book.book_id", "=", $book->id)->getModels();
            $cartItem->book = $book;
        }

        return $cartItems;
    }

    public function getCartItemBooks(string $id) {
        // todo get book objects from the cart items with the order id
        $books = CartItem::query()
            ->where('cart_items.order_id', $id)
            ->get();

        return $books;
    }

    public function changeStatus(Request $request): JsonResponse
    {
        $orderId = $request->get('id');
        $status = $request->get("status");
        $comment = $request->get("comment");

        try {
            $order = Order::query()->where('id', $orderId)->first();

            if ($order != null) {
                $orderStatus = new OrderStatus(['order_id' => $orderId, 'status' => $status, 'comment' => $comment]);
                $orderStatus->save();

                $order->status = $status;
                $order->save();

            }

            return response()->json($order, 201);
        } catch (Exception $e) {
            // rollback all queries
            return response()->json("changing status failed: " . $e->getMessage(), 420);
        }
    }

    public function getStatusHistory (string $id){
        $statuses = OrderStatus::query()->where('order_id',$id)->get();
        return $statuses;
    }

    public function getBookPrice(string $id){
        $price = CartItem::query()->where('book_id', $id)
            ->join('books', 'cart_items.book_id', "=", 'books.id')
            ->get('price');
        return $price;
    }

    public function getTax (string $id) {
            $order = Order::query()->where('id', $id)->first();

            if ($order != null) {
                // $request = $this->parseRequest($request);
                // $order->update($request->all());
                $tax = Order::query()
                    ->where('orders.id', $id)
                    ->join('users', 'orders.user_id', '=', 'users.id')
                    ->join('addresses', 'users.address_id', '=', 'addresses.id')
                    ->join('countries', 'addresses.country_id', '=', 'countries.id')
                    ->select('tax')
                    ->get();
                return $tax;
            }

}

    protected function addTax(Order $order)
    {
        $tax = $order->user()->getResults()->address()->getResults()->country()->getResults()->tax;

        //->get();
        // $tax = $this->getTax($id
        $newTotal = $order->total / 100;
        $order->total = $order->total + ($newTotal * $tax);
        $order->save();
    }

    public function newOrder(Request $request){
        $items = $request->get('items');
        if(empty($items)) {
            return response()->json("no items given", Response::HTTP_BAD_REQUEST);
        }

        try {
            // $cartItems = [];
            $userId = 0;
            $bookId = 0;

            foreach($items as $id => $item) {
                $userId = $item['userId'];
                $bookId = $id;
            }

            $order  = new Order(["date" => now(), "total" => 0, "status" => "open", "user_id" => $userId]);
            $order->save();

            $status = new OrderStatus( ['order_id' => $order->id, 'status' => $order->status, 'comment' => 'Neue Betellung']);
            $status->save();

            $total = 0;

            foreach($items as $id => $item) {
                $book = Book::query()->where("id", "=", $id)->first();

                if(!$book) {
                    continue;
                }

                $cartItem = new CartItem(['quantity' => $item['quantity'], 'order_id' => $order->id, 'book_id' => $book->id]);
                $cartItem->save();

                if($book) {
                    $total += $book->price;
                }
            }

            $order->total = $total;
            $this->addTax($order);

            return response()->json($order, 201);
        }
        catch (Exception $e) {
            return response()->json("adding taxes failed: " . $e->getMessage(), 420);
        }
    }

}
