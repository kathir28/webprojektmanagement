<?php

namespace App;

use App\CartItem;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ShoppingCart extends Model
{
    protected $fillable = [
        'id', 'date', 'total',
        'user_id',
        //Netto, Brutto
    ];

    public function user(): BelongsTo{
        return $this->belongsTo('App\User','user_id');
    }
    public function cartItems(): HasMany{
        return $this->hasMany('App\CartItem','shopping_cart_id');
    }
}