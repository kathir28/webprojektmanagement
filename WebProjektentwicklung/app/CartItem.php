<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\User;
use App\Order; 
use App\Book;

class CartItem extends Model
{
    // alle beschreibbaren properties
    protected $fillable = [
        'id', 'quantity', 'order_id', 'book_id',
    ];

    public function order(): BelongsTo{
        return $this->belongsTo(Order::class);
    }

    public function book(): BelongsTo{
        return $this->belongsTo(Book::class);
    }

}
