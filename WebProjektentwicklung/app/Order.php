<?php

namespace App;

use App\CartItem;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Order extends Model
{
    protected $fillable = [
        'id', 'date', 'total', 'status', 'user_id'
        //Netto, Brutto
    ];

    public function cartItems(): HasMany {
        return $this->hasMany(CartItem::class);
    }

    public function user() : BelongsTo  {
        return $this->belongsTo(User::class);
    }
}