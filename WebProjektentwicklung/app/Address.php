<?php

namespace App;

use App\User;
use App\Country;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Address extends Model
{
    protected $fillable = [
        'id', 'street', 'plz', 'town', 'country_id'
    ];

    public function country() : BelongsTo  {
        return $this->belongsTo(Country::class);
    }

    public function users(): HasMany{
        return $this->hasMany(User::class);
    }
}