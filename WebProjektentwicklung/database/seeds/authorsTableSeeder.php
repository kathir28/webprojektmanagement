<?php

use Illuminate\Database\Seeder;

class authorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author1 = new \App\Author();
        $author1->firstName = 'J.R.R.';
        $author1->lastName = 'Tolkien';
        $author1->save();

        $author2 = new \App\Author();
        $author2->firstName = 'J.K.';
        $author2->lastName = 'Rowling';
        $author2->save();

        $author3 = new \App\Author();
        $author3->firstName = 'Heinz';
        $author3->lastName = 'Gruber';
        $author3->save();
    }
}
