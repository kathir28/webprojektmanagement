<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Address;

class usersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $address = Address::all()->first();

        $user = new User();
        $user->name = 'adminuser';
        $user->firstname = 'Maria';
        $user->lastname = 'Musterfrau';
        $user->email = "admin@gmail.com";
        $user->password = bcrypt('secret');
        $user->admin = true;
        $user->address()->associate(Address::find(1));
        $user->save();

        $user2 = new User();
        $user2->name = 'testuser';
        $user2->firstname = 'Mario';
        $user2->lastname = 'Mustermann';
        $user2->email = "test1@gmail.com";
        $user2->password = bcrypt('secret');
        $user2->admin = false;
        $user2->address()->associate(Address::find(2));
        $user2->save();

        $user2 = new User();
        $user2->name = 'testuser2';
        $user2->firstname = 'Martin';
        $user2->lastname = 'Huber';
        $user2->email = "test2@gmail.com";
        $user2->password = bcrypt('secret');
        $user2->admin = false;
        $user2->address()->associate(Address::find(2));
        $user2->save();
    }
}
