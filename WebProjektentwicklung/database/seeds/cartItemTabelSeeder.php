<?php

use Illuminate\Database\Seeder;
use App\CartItem;

class cartItemTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = App\Order::all()->first();
        $book = App\Book::all()->first();

        $cartItem = new \App\CartItem();
        $cartItem->quantity = 3;
        $cartItem->order()->associate($order);
        $cartItem->book()->associate($book);
        $cartItem->save();

        $cartItem1 = new \App\CartItem();
        $cartItem1->quantity = 2;
        $cartItem1->order()->associate($order);
        $cartItem1->book()->associate($book);
        $cartItem1->save();
    }
}

