<?php

use Illuminate\Database\Seeder;
use App\Order;

class ordersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::all()->first();
        // $cartItem = App\CartItem::all()->first();
        // $ad  = App\User::pluck('title') ;

        $order = new Order;
        $order->date = new DateTime();
        $order->total = 25;
        $order->status = 'offen';
        $order->user()->associate($user);
        $order->save();
    }
}
