<?php

use Illuminate\Database\Seeder;
use App\Address;

class addressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = App\Country::all()->first();

        $address = new Address;
        $address->street = 'Bahnhofstraße';
        $address->plz = 4232;
        $address->town = "Hagenberg";
        $address->country()->associate($country);
        $address->save();

        $address = new Address;
        $address->street = 'Bahnhofstraße';
        $address->plz = 4230;
        $address->town = "Pregarten";
        $address->country()->associate($country);
        $address->save();

    }
}
