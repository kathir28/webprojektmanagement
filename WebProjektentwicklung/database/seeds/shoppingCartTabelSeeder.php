<?php

use Illuminate\Database\Seeder;
use App\ShoppingCart;

class shoppingCartTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::all()->first();

        $shoppingCart = new \App\ShoppingCart();
        $shoppingCart->total = 0;
        $shoppingCart->user()->associate($user);;
        $shoppingCart->save();
    }
}
