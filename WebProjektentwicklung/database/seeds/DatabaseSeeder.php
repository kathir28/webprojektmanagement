<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(countriesTableSeeder::class);
        $this->call(addressesTableSeeder::class);
        $this->call(usersTableSeeder::class);
        $this->call(authorsTableSeeder::class);
        $this->call(booksTableSeeder::class);
        $this->call(ordersTableSeeder::class);
        $this->call(cartItemTabelSeeder::class);
        // $this->call(shoppingCartTabelSeeder::class);
        $this->call(orderStatusTableSeeder::class);


    }
}
