<?php

use Illuminate\Database\Seeder;
use App\Country;

class countriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $country = new Country;
        $country->name = 'Österreich';
        $country->tax = 20;
        $country->save();

        $country2 = new Country;
        $country2->name = "Deutschland";
        $country2->tax = 10;
        $country2->save();

    }
}
