<?php

use Illuminate\Database\Seeder;
use App\Book;

class booksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::all()->first();

        $book = new Book;
        $book->title = "Herr der Ringe";
        $book->subtitle = "Die Gefährten";
        $book->isbn = "4321567890";
        $book->rating = 9;
        $book->description = 'Erster Teil';
        $book->published = new DateTime();
        $book->price = 23;
        //map exsisting user to book
        $book->user()->associate($user);
        $book->save();

        $authors = App\Author::all()->pluck('id');
        $authors = App\Author::all()->first();
        $book->authors()->sync($authors);
        $book->save();

        $book2 = new Book;
        $book2->title = "Herr der Ringe 2";
        $book2->subtitle = "Die zwei Türme";
        $book2->isbn = "1234567890";
        $book2->rating = 10;
        $book2->description = 'Zweiter Teil';
        $book2->published = new DateTime();
        $book2->price = 25;
        $book2->user()->associate($user);
        $book2->save();

        $image1 = new \App\Image();
        $image1->title = 'Cover1';
        $image1->url = 'https://www.klett-cotta.de/media/1/9783608939811.jpg';
        $image1->book()->associate($book);
        $image1->save();

        $image2 = new \App\Image();
        $image2->title = 'Cover2';
        $image2->url = 'https://images-na.ssl-images-amazon.com/images/I/51smfUvItSL._AC_UL320_SR206,320_.jpg';
        $image2->book()->associate($book2);
        $image2->save();

        $authors2 = App\Author::all()->pluck('id');
        $authors2 = App\Author::all()->first();
        $book2->authors()->sync($authors2);
        $book2->save();

        $book3 = new Book;
        $book3->title = "Herr der Ringe 3";
        $book3->subtitle = "Die Rückkehr des Königs";
        $book3->isbn = "0234567891";
        $book3->rating = 8;
        $book3->description = 'Dritter Teil';
        $book3->published = new DateTime();
        $book3->price = 27;
        $book3->user()->associate($user);
        $book3->save();

        $image3 = new \App\Image();
        $image3->title = 'Cover1';
        $image3->url = 'https://images-na.ssl-images-amazon.com/images/I/51SmBvM5SBL._AC_UL320_SR206,320_.jpg';
        $image3->book()->associate($book3);
        $image3->save();

        $authors3 = App\Author::all()->pluck('id');
        $authors3 = App\Author::all()->first();
        $book3->authors()->sync($authors3);
        $book3->save();

        $book4 = new Book;
        $book4->title = "Harry Potter I";
        $book4->subtitle = "Die Stein der Weisen";
        $book4->isbn = "2345678901";
        $book4->rating = 9;
        $book4->description = 'Harry Potter Teil 1';
        $book4->published = new DateTime();
        $book4->price = 15;
        $book4->user()->associate($user);
        $book4->save();

        $image4 = new \App\Image();
        $image4->title = 'Harry Potter 1';
        $image4->url = 'https://exlibris.azureedge.net/covers/9783/5513/5401/3/9783551354013xxl.jpg';
        $image4->book()->associate($book4);
        $image4->save();

        $authors4 = App\Author::where('id','=','2')->get();
        //$authors4 = App\Author::all()->pluck('2);
        $book4->authors()->sync($authors4);
        $book4->save();

        $book5 = new Book;
        $book5->title = "Harry Potter II";
        $book5->subtitle = "Und die Kammer des Schreckens";
        $book5->isbn = "3456789012";
        $book5->rating = 8;
        $book5->description = 'Harry Potter Teil 2';
        $book5->published = new DateTime();
        $book5->price = 18;
        $book5->user()->associate($user);
        $book5->save();

        $image5 = new \App\Image();
        $image5->title = 'Harry Potter 2';
        $image5->url = 'https://images-na.ssl-images-amazon.com/images/I/51HBXxbREBL._SX319_BO1,204,203,200_.jpg';
        $image5->book()->associate($book5);
        $image5->save();

        $authors5 = App\Author::where('id','=','2')->get();
        //$authors4 = App\Author::all()->pluck('2);
        $book5->authors()->sync($authors5);
        $book5->save();

    }
}
