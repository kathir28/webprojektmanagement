<?php

use Illuminate\Database\Seeder;
use App\OrderStatus;

class orderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = App\Order::all()->first();

        $status = new OrderStatus;
        $status->order()->associate($order);
        $status->status = "offen";
        $status->comment ="Neue Beschreibung";
        $status->save();
    }
}
